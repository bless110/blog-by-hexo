---
title: 微信小程序如何隐藏canvas
date: 2019-05-23 16:27:31
tags: 微信小程序
---

问题：选中图片后，把图片画到`canvas`上，但是并不想让用户看到这个`canvas`，请问如何在小程序页面中隐藏`canvas`呢？
<!-- more -->
解决方案（已验证有效）：为canvas设置如下样式
```css
{
    postion: fixed;
    left: -100%;
    bottom: -100%;
}
```