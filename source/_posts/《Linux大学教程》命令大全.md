---
title: 《Linux大学教程》命令大全
date: 2019-12-03 11:03:42
tags:
---

`date`
显示当前日期和时间

`who`
显示当前登录系统的所有用户标识（同时显示终端名称、登陆时间等）

`passwd`
改变口令

`last`
查看所有用户登录记录

`last [用户标识]`
查看某个用户的登录记录

*<!-- more -->*

`exit`
退出当前登录的身份，返回上一个登录身份

`su [用户标识]`
临时以另一个用户身份（用户标识默认为root）登录（不改变环境）

`su - [用户标识]`
临时以另一个用户身份登录（同时改变环境）

`sudo`
临时以超级用户身份登录

`less`
每次一屏地显示内容

`sudo init`
修改运行级别

`sudo reboot`
重启

`sudo shutdown now` 
立即关机

`dmesg`
显示系统启动信息

`infocmp`
显示当前正在使用的终端信息

`echo $TERM`
显示当前正在使用的终端类型

`printenv`
显示当前shell所有环境变量

`stty -a`
显示系统键盘的映射

`stty`
只显示默认值已经被改变的键盘映射

`stty kill ^U`
将kill信号映射到<ctrl+u>键盘组合键

`which`
查看某个命令是否可用

`date -u`
显示UTC时间

`cal`
显示当前月份日历

`cal 2019`
显示2019年全部月份日历

`cal 1 2019`
显示2019年1月份的日历

`cal -j 12 2019`
显示2019年12月份的日历（不显示日期，而是显示这一天是这一年的第几天。技巧：如果12月的最后一天为改年该第366天，则可判断该年为闰年，否则为平年）

`calendar foo.txt`
根据文件foo.txt创建提醒任务

`uptime`
显示系统连续运行多长时间的相关信息

`hostname`
查看计算机名称

`uname`
查看操作系统的名称

`uname -a`
查看操作系统的详细信息

`whoami`
显示当前用户的标识

`users`
仅仅显示当前登陆系统的所有用户标识

`w`
查看系统上用户更多信息

`w [用户标识]`
查看该用户正在做什么

`lock`
临时锁住终端（默认15分钟）

`lock -5`
临时锁住终端5分钟

`leave 1015`
在10点15分提醒我离开

`leave +15`
15分钟后提醒我离开

`bc`
启动计算器

`man [命令名称]`
查看命令的文档资料

`whatis [命令名称]`
查看命令简介

`apropos manual`
查找某些命令，它们的文档资料包括关键词manual（不区分大小写）

`info [命令名称]`
显示命令的info文件

`date;cal`
同时执行date和cal两个命令

`env或printenv`
显示所有环境变量

`set`
显示所有shell变量（局部变量）

`set -o [shell变量]`
关闭shell变量

`set +o [shell变量]`
打开shell变量

`export DEMO=value`
将变量DEMO导出到环境变量

`type [命令名称]`
查看命令是不是shell内置命令

`export PATH="$PATH:$HOME/bin"`
将搜索路径的值修改为旧值加上$HOME/bin

`echo "my name is ${USER}"`
在字符串中使用变量

echo "The time and date are \`date\`.
在字符串中嵌入date命令

`export PS1="\A:\$"`
修改shell提示，$符号前面新增时间

`fc -l`
查看使用过的命令列表

`fc -s 20`
重新执行编号为20的命令（如果不指定编号，默认为上一个命令）

`fc -s stringA=stringB 20`
重新执行编号为20的命令，并且把之前的字符串stringA改为字符串stringB

`export HISTORY=50`
指定历史列表最多存放50条命令

`alias`
显示所有的别名

`alias dt=date`
将date命令映射到到别名dt

`alias dt`
显示别名dt代表的含义

`unalias dt`
删除别名dt

`unalias -a`
删除所有别名

`\ls`
运行实际命令ls本身，而不是别名

`more data.txt`
每次8一屏地显示data.txt中的数据，按空格键会显示下一屏数据。

`> out.txt`
将数据写入到文件out.txt中，如果文件不存在则创建，如果文件中有内容则覆盖内容。

`>> out.txt`
将数据追加到文件out.txt的尾部。

`set -o noclobber`
防止文件内容被意外地覆盖。执行此命令后，如果标准输出所指定的文件已存在，屏幕会提示错误。如果你确实希望覆盖，用`>|`代替`>`

`sort 1> output.txt 2> error.txt`
将标准输出写入output.txt文件，标准错误写入error.txt文件。

`sort &> output.txt` 或 `sort >& output.txt`
将标准输出和标准错误都写入output.txt文件

`sort > /dev/null`
不想看标准输出，将标准输出抛弃

`sort 2> /dev/null`
不想看标准错误，将标准错误抛弃

`sort 2> /dev/null`
不想看标准错误，将标准错误抛弃

![Ybz061.png](https://s1.ax1x.com/2020/05/21/Ybz061.png)
> 未完待续