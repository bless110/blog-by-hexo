---
title: JSON.parse实现深拷贝的局限性
tags: javascript
---

1. 可以用这个技巧进行数据对象的拷贝
2. 如果被拷贝的对象中有function，则拷贝之后的对象就会丢失这个function
3. 如果被拷贝的对象中有正则表达式，则拷贝之后的对象正则表达式会变成Object