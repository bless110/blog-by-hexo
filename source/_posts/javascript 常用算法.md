---
title: javascript 常用算法
tags: 算法
---

### 简单选择排序

```javascript
function selectSort(arr) {
    var len = arr.length
    for (var x = 0; x < len - 1; x++) {
        var min = x;
        for (var y = x + 1; y < len; y++) {
            if (arr[min] > arr[y]) {
                min = y
            }
        }
        if (min !== x) {
            var temp = arr[x];
            arr[x] = arr[min];
            arr[min] = temp;
        }
    }
    return arr
}
```

<!-- more -->

### 冒泡排序

```javascript
function bubbleSort(arr){
     for(var i=1;i<arr.length;i++){
         for(var j=0;j<arr.length-i;j++){
             var temp;
             if(arr[j]>arr[j+1]){
                 temp=arr[j];
                 arr[j]=arr[j+1];
                 arr[j+1]=temp;
             }
         }
     }
     return arr;
}
```
### 插入排序

```javascript
function insertionSort(arr){
    for (var i = 1; i < arr.length; i++) {
        var j = i;
        while (j > 0 && arr[j] < arr[j - 1]) {
            var temp = arr[j];
            arr[j] = arr[j - 1];
            arr[j - 1] = temp;
            j--;
        }
    }
}
```

### 随机指定长度字符串

```javascript
function randomString(n) {
    var str = 'abcdefghijklmnopqrstuvwxyz9876543210';
    var tmp = '';
    var l = str.length;
    for(var i = 0; i < n; i++) {
        tmp += str.charAt(Math.floor(Math.random() * l));
    }
    return tmp;
}
```