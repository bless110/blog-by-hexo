---
title: 联想法区分substring和substr方法
tags: javascript
---
`substring`和`substr`方法都可以从字符串中抽取出一些字符，无论是方法名和参数，都十分相似。在紧张的面试氛围下，老铁们可能会混淆两者传入的第二个参数。笔者遵循联想法，想出了一个区分方法。
<!-- more -->
先看看这两个方法各自的参数：
```javascript
stringObject.substr(start,length)
stringObject.substring(start,stop)
```
两者区别于第二个参数。`substr`传的是长度（length），`substring`传的是结束点（stop）。
str和length勉勉强强能组成单词strength（强度；力量）而string和length没法组成单词。
所以和方法`substr`搭配的参数应该是长度length。
以后面试官问你`substr`或者开发中使用`substr`时，脑海里就能准确地想出第二个参数为长度length。