---
title: 微信小程序音频audio API如何结合列表渲染
date: 2019-03-10 12:33:52
tags:
---
`audio`组件在某些苹果手机存在致命bug，点击组件无任何反应。从1.6.0 版本开始，官方不再维护`audio`组件，建议使用能力更强的 `wx.createInnerAudioContext` 接口。以前列表渲染出来的`audio`需要技术改造。

<!-- more -->

比如我们有一段音频数据：
```javascript
var data = [{
  id:1,
  title:'爱过的人我已不再拥有',
  image:'http://wimg.spriteapp.cn/picture/2016/0904/57cb989b04d4c.png',
  src:'http://wvoice.spriteapp.cn/voice/2016/0904/57cb989b1e3b4.mp3'
},{
  id:2,
  title:'已过了耳濡目染的年',
  image:'http://wimg.spriteapp.cn/picture/2016/0517/573b1240af3da.jpg',
  src:'http://wvoice.spriteapp.cn/voice/2016/0517/573b1240d0118.mp3'
},{
  id:3,
  title:'感觉很放松',
  image:'http://wimg.spriteapp.cn/picture/2016/1108/5821463c3fad8.jpg',
  src:'http://wvoice.spriteapp.cn/voice/2016/1104/581b63392f6cb.mp3'
}]
```
现在我们想要达到这种目的：

1. 音频以列表的形式展现在小程序上。
2. 每个音频旁边都会有个按钮，初始情况按钮上文字为‘播放’；点击‘播放’，小程序播放该音频，按钮文字变成‘暂停’。
3. 点击‘暂停’按钮，停止播放音频，按钮文字变成‘播放’。
4. 不能同时播放多个音频：如果音频A正处于播放状态，点击播放音频B时，音频A停止播放。


思路：
1. 使用`wx:for`
2. 将列表中每一个音频做为组件
3. 在父组件设置一个`id`，用于控制应该播放哪个音频

示例代码
[https://github.com/zhaohaodang/wechat-app-audio-list](https://github.com/zhaohaodang/wechat-app-audio-list)


