---
title: 梳理cookie
tags: javascript
---
### cookie 概述
cookie 是JavaScript提供的一种本地存储机制，是存于硬盘某一位置的文本文件。以win7 Chrome 为例，其cookie的存储位置为`C:\Users\yourUserName\AppData\Local\Google\Chrome\UserData\Default\Cookies`
打开开发者工具，在Application->Storage->Cookies标签下，可以查看与网站相关的cookie。
当cookie读/写时，会被对应地解码/编码。
<!-- more -->
### cookie 属性
**名称**：cookie的唯一标识符，不区分大小写（例如demo与Demo指向相同的cookie）。
**值**：储存在cookie 中的字符串值。
**域**：是指cookie 对于哪个域是有效的，默认为设置cookie 的那个网站的域。比如将某一cookie的域设置为`.demo.com`，那么，对所有该域（例如域名为`api.demo.com`的后端接口）发送的请求，都会携带这个cookie信息。
**路径**：对于指定域中的路径发送cookie信息，例如，指定路径为 `http://www.demo.com/login`，那么请求 `http://www.demo.com/` 的页面就不会发送cookie信息，即使请求都是来自同一个域的。
**失效时间**：表示cookie何时被删除的时间戳，默认浏览器会话结束时即将所有cookie 删除。可以自己设置删除时间。这个值是个GMT 格式的日期（Wdy, DD-Mon-YYYY HH:MM:SS GMT），用于指定应该删除cookie 的准确时间。如果设置的失效日期是个以前的时间，则cookie 会被立刻删除。
**安全标志**：指定后，cookie 只有在使用SSL 连接的时候才发送到服务器。例如，cookie 信息只能发送给 `https://www.demo.com`，而 `http://www.demo.com` 的请求则不能发送 cookie。
### cookie 优点
**数据持久**：刷新页面，仍然存在有效的cookie。
**多页面共享数据**：将不同页面需要共享的数据写入cookie，是种不错的选择。
**信息验证**：请求服务端会携带对应的有效cookie，可核查信息。
**保持登陆状态**：像论坛这类具有登陆功能的网站，会在用户登陆成功后写入相关cookie以维持用户的登陆状态，直到cookie被销毁。
### cookie 限制
**禁用cookie**：将浏览器设置为禁用cookie会禁止cookie相关操作。
**数量限制**：每个域的cookie总量是有限的，cookie超出限制会导致浏览器删除陈旧的cookie，腾出空间给新设置的cookie。
**cookie可被篡改**：cookie可以被脚本修改，这就需要某些加密手段对cookie进行加密处理。
### cookie 操作
#### 原生js实现
创建cookie

```javascript
function setCookie(name,value,expiredate,domain,path,secure){
    var cookieText=escape(name)+"="+escape(value);
    if(expiredate){
        var exdate=new Date();
        exdate.setDate(exdate.getDate()+expiredate);
        cookieText+=";expires="+exdate.toGMTString();
    }
    if(domain){
        cookieText+=";domain="+domain;
    }
    if(path){
        cookieText+=";path="+path;
    }
    if(secure){
        cookieText+=";secure";
    }
    document.cookie=cookieText;
}
```
> 必须传入参数name和value

根据name获取对应cookie
```javascript
    function getCookie(c_name) {
        if (document.cookie.length > 0) {
            var c_start = document.cookie.indexOf(c_name + "=")
            if (c_start != -1) {
                c_start = c_start + c_name.length + 1
                var c_end = document.cookie.indexOf(";", c_start)
                if (c_end == -1) c_end = document.cookie.length
                return unescape(document.cookie.substring(c_start, c_end))
            }
        }
        return ""
    }
```
删除cookie
```javascript
function deCookie(name,value,expiredate,domain,path,secure){
    this.setCookie(name,"",new Date(0),domain,path,secure);
}
```
#### jquery-cookie 插件 实现
```javascript
//创建名为name值为value，有效期为7天，路径为`/`的cookie
$.cookie('name', 'value', { expires: 7, path: '/' });
//获取指定name的cookie，若找不到，返回 undefined
$.cookie('name'); // => "value"
$.cookie('nothing'); // => undefined
//获取所有有效cookie
$.cookie();
//删除指定cookie，删除失败返回false
$.removeCookie('name'); // => true
$.removeCookie('nothing'); // => false
//若创建某一cookie时设置了额外信息，删除时需要传入对应的额外参数
$.cookie('name', 'value', { path: '/' });
// 删除失败
$.removeCookie('name'); // => false
// 删除成功
$.removeCookie('name', { path: '/' }); // => true
```
### cookie 如何读写json数据
写入json数据：调用`JSON.stringify()`方法，将json数据转换为字符串，利用上面的cookie操作创建cookie。

读取json数据：读取该cookie时，返回json字符串，调用`JSON.parse()`方法，解析为原json数据。