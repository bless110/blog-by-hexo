---
title: 使用v2ray免费翻墙Mac版【详细图文教程】
date: 2019-10-11 09:40:16
tags:
---
> 更新时间 2019-10-11
## 1.简介
V2Ray 是⼀个与 Shadowsocks 类似的代理软件，可以⽤来科学上⽹（翻墙）学习国外先进科学技
术。
<!-- more -->
## 2.安装客户端
Mac版客户端可以选择 v2rayX，只要下载⼀个.app⽂件。
下载链接： [https://github.com/Cenmrev/V2RayX/releases](https://github.com/Cenmrev/V2RayX/releases)
![uHe2Ae.png](https://s2.ax1x.com/2019/10/11/uHe2Ae.png)
## 3.配置客户端
1. 下载完成后直接打开 .app ⽂件，可能会有授权弹窗，打开后导航栏会出现 v2rayX 的 logo。
![uHmKHO.png](https://s2.ax1x.com/2019/10/11/uHmKHO.png)
2. 点击 *Configure…* 在 *VMess Servers* 区域新增⼀个服务。
![uHmJgI.png](https://s2.ax1x.com/2019/10/11/uHmJgI.png)
3. 点击右下⻆的 *Import…* 选择 *Import from other links…*
![uHmB5Q.png](https://s2.ax1x.com/2019/10/11/uHmB5Q.png)
4. 在对话框中输⼊链接 https://jiang.netlify.com/ （这个链接是⽹友共享的，⾥⾯包含了很多节点）
![uHmT2R.png](https://s2.ax1x.com/2019/10/11/uHmT2R.png)
5. 点击 OK 按钮后会发现多了很多节点，⼀般只会⽤其中⼀两个，没⽤的删掉，我只保留了⾹港3、 4节点。
![uHuukD.png](https://s2.ax1x.com/2019/10/11/uHuukD.png)
6. 点击右下⻆的 OK 按钮，如果提示 *tag is not valid* ，请检查每个节点的tag是否重复，是否没填写。
7. 点击导航栏的 logo ，选择 *Global Mode* ， Server 下选择刚刚添加的节点，⽐如⾹港3。
![uHKha8.png](https://s2.ax1x.com/2019/10/11/uHKha8.png)
8. 最后⼀步，选择 *Rounting Rule* 下的 *all_to_main* ，这步很重要。
![uHKqrq.png](https://s2.ax1x.com/2019/10/11/uHKqrq.png)